﻿
using System.Windows;
using System.Windows.Input;

namespace RegisterPage
{
    /// <summary>
    /// MainWindow.xaml 的交互逻辑
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void Border_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.ChangedButton == MouseButton.Left)//如果按下了鼠标左键
            {
                this.DragMove();//允许拖动该窗口
            }
        }
    }
}
